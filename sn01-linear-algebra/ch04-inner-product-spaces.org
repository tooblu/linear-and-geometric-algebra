* 4 Inner Product Spaces
** 4.1 Oriented Lengths
   - Definition 4.1 (Inner product)\\
     The /inner product/ of oriented lengths *u* and *v* is the /scalar/
     #+begin_center
     \(\mathbf{u} \cdot \mathbf{v} = |u||v| \cos \theta, 0 \leq \theta \leq \pi,\)
     #+end_center
     where $\theta$ is the angle between the vectors placed tail-to-tail.
   Picture what happens as $\theta$ goes from 0 to $\pi$.
   Note this definition is coordinate /free/; it is in /geometric/ terms.
   - Exercise 4.1 (Norm). Show that the norm $|\mathbf{v}|$ of an oriented length can be expressed in  terms of the inner product:
     #+begin_center
     \(|\mathbf{v}|^2 = \mathbf{v} \cdot \mathbf{v}\)
     #+end_center
     \begin{align}
     \mathbf{v} \cdot \mathbf{v} &= |\mathbf{v}| |\mathbf{v}| \cos \theta \text{, where $\theta$ is the angle between $\mathbf{v}$ and itself.} \notag \\
     &= |\mathbf{v}| |\mathbf{v}| 1\text{, the angle between $\mathbf{v}$ and itself is 0, and $\cos 0 = 1$} \notag \\
     &= |\mathbf{v}|^2 \notag
     \end{align}
   - Exercise 4.2 Suppose that $\mathbf{u} \cdot \mathbf{v} = 4$. What is $2 \mathbf{u} \cdot 6 \mathbf{v}$? Explain.
     - 48
     - The scalar multiplication does not change the angle between the vectors, only their lengths, so $\cos \theta$ does not change.
     \begin{align}
     2 \mathbf{u} \cdot 6 \mathbf{v} &= |2 \mathbf{u}| |6 \mathbf{v}| \cos \theta \notag \\
     &= 2 |\mathbf{u}| 6 |\mathbf{v}| \cos \theta \notag \\
     &= 12 |\mathbf{u}| |\mathbf{v}| \cos \theta \notag \\
     &= 12 \cdot 4 \notag \\
     &= 48 \notag
     \end{align}
   - Exercise 4.3 Compute $2 \mathbf{e}_1 \cdot (2 \mathbf{e}_1 + 2 \sqrt{3} \mathbf{e}_2)$ from Definition 4.1.
     Here $\mathbf{e}_1$ and $\mathbf{e}_2$ are perppendicular vectors of length one.
     \begin{align}
     2 \mathbf{e}_1 \cdot (2 \mathbf{e}_1 + 2 \sqrt{3} \mathbf{e}_2) &= 2 \mathbf{e}_1 \cdot 2 \mathbf{e}_1 + 2 \mathbf{e}_1 \cdot 2 \sqrt{3} \mathbf{e}_2) \notag \\
     &= 4 (\mathbf{e}_1 \cdot \mathbf{e}_1) + 4 (\mathbf{e}_1 \cdot \sqrt{3} \mathbf{e}_2) \notag \\
     &= 4 (\mathbf{e}_1 \cdot \mathbf{e}_1 + \mathbf{e}_1 \cdot \sqrt{3} \mathbf{e}_2) \notag \\
     &= 4 (1 + 0) \notag
     \end{align}
     , since the angle between $\mathbf{e}_1$ and itself is 0, and the angle between $\mathbf{e}_1$ and $\mathbf{e}_2$ is $\pi/2$.
   - Normalize. Dividing a vector $\mathbf{v} \neq \mathbf{0}$ by its norm $|\mathbf{v}|$ /normalizes/ it to norm 1.\\
     A vector *v* with $|\mathbf{v}| = 1$ is called a /unit vector/.
   - Projection. $\mathsf{P}_\mathbf{u}(\mathbf{v})$ is the /projection/ of the oriented length *v* on the oriented length *u*.
   - Theorem 4.2 (Projection formula). $\mathsf{P}_\mathbf{u}(\mathbf{v}) = \frac{\mathbf{u} \cdot \mathbf{v}}{|\mathbf{u}^2|} \mathbf{u}$
   - Theorem 4.3 (Inner product properties). Let *u*, *v*, and *w* be oriented lenghts and /a/ be a scalar. Then\\
     \begin{enumerate}
       \item $(a\mathbf{u}) \cdot \mathbf{v} = a(\mathbf{u} \cdot \mathbf{v})$
       \item $(\mathbf{u} + \mathbf{v}) \cdot \mathbf{w} = \mathbf{u} \cdot \mathbf{w} + \mathbf{v} \cdot \mathbf{w}$
       \item $\mathbf{u} \cdot \mathbf{v} = \mathbf{v} \cdot \mathbf{u}$
       \item if $\mathbf{v} \neq \mathbf{0}$, then $\mathbf{v} \cdot \mathbf{v} > 0$.
     \end{enumerate}
*** Orthogonal Vectors
   - Definition 4.4 (Orthogonal vectors). For two nonzero oriented lengths, "orthogonal" is a synonym of "perpendicular".\\
     Also define $\mathbf{0}$ to be orthogonal to every vector. (Why? It is convienient.)
   - Theorem 4.5 (Orthogonal vectors test). $\mathbf{u}$ and $\mathbf{v}$ are orthogonal $\iff$ $\mathbf{u} \cdot \mathbf{v} = 0$.
   - Exercise 4.4 Show that if $|\mathbf{u}| = |\mathbf{v}|$, then $\mathbf{u} + \mathbf{v}$ and $\mathbf{u} - \mathbf{v}$ are orthogonal. Draw a diagram for this.\\
     \begin{align}
     (\mathbf{u} + \mathbf{v}) \cdot (\mathbf{u} - \mathbf{v}) &= \mathbf{u} \cdot (\mathbf{u} - \mathbf{v}) + \mathbf{v} \cdot (\mathbf{u} - \mathbf{v}) \notag \\
     &= \mathbf{u} \cdot \mathbf{u} - \mathbf{u} \cdot \mathbf{v} + \mathbf{v} \cdot \mathbf{u} - \mathbf{v} \cdot \mathbf{v} \notag \\
     &= \mathbf{u} \cdot \mathbf{u} - \mathbf{u} \cdot \mathbf{v} + \mathbf{u} \cdot \mathbf{v} - \mathbf{v} \cdot \mathbf{v} \notag \\
     &= \mathbf{u} \cdot \mathbf{u} - \mathbf{v} \cdot \mathbf{v} \notag \\
     &= |\mathbf{u}|^2 - |\mathbf{v}|^2 \notag \\
     &= |\mathbf{u}|^2 - |\mathbf{u}|^2 \notag \\
     &= 0 \notag
     \end{align}
     therfore, $\mathbf{u}$ and $\mathbf{v}$ are orthogonal.
   - Orthornormal basis.
*** Coordinates
   - Theorem 4.6 (Formula for $\mathbf{u} \cdot \mathbf{v}$). Let $\mathbf{u}$ and $\mathbf{v}$ be vectors in $L^3$.
     Expand $\mathbf{u}$ and $\mathbf{v}$ with respect to an orthonormal basis $\{\mathbf{e}_1, \mathbf{e}_2, \mathbf{e}_3\}$:
     $\mathbf{u} = u_1 \mathbf{e}_1 + u_2 \mathbf{e}_2 + u_3 \mathbf{e}_3$ and
     $\mathbf{v} = v_1 \mathbf{e}_1 + v_2 \mathbf{e}_2 + v_3 \mathbf{e}_3$.
     Then
     \begin{equation}
     \mathbf{u} \cdot \mathbf{v} = u_1 v_1 + u_2 v_2 + u_3 v_3
     \end{equation}
*** Problems 4.1
    - 4.1.1
    - 4.1.2
    - 4.1.3
** 4.2 Rn
*** Problems 4.2
    - 4.2.1
    - 4.2.2
** 4.2 Inner Product Spaces
*** Norm
*** Angle
*** Problems 4.3
    - 4.3.1
    - 4.3.2
    - 4.3.3
    - 4.3.4
    - 4.3.5
    - 4.3.6
    - 4.3.7
    - 4.3.8
    - 4.3.9
    - 4.3.10
    - 4.3.11
    - 4.3.12
    - 4.3.13
    - 4.3.14
    - 4.3.15
    - 4.3.16
** 4.2 Orthogonality
*** Orthonormal Bases
*** Gram-Schmidt Orthogonalization
*** The Orthogonal Complement
*** Least Squares
*** Problems 4.4
    - 4.4.1
    - 4.4.2
    - 4.4.3
    - 4.4.4
    - 4.4.5
    - 4.4.6
    - 4.4.7
    - 4.4.8
    - 4.4.9
    - 4.4.10
    - 4.4.11
    - 4.4.12
    - 4.4.13
    - 4.4.14
